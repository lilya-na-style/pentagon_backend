# README #


## 1) Authorization page ##
>>* mapping: /auth
>>* http POST-request
>>* format JSON 
>>* Validation
>> String email = user's email (not empty, not null)
>> String password = user's password (not empty, not null, first capital letter, 8-16 symbols, numbers required, special characters forbidden) 



***
If authorization is successful
- 

## Request body:  ##

>>{ 

>>"email": "lily@gmail.com",

>>"password": "Qwerty123"

>>}

Successful request:

- Status: OK 

## Response body:  ##

>>{ 

>>"code": 200,

>>"type": "unknown",

>>"message": "logged in user session:1643304005288" 

>>}

***
If authorization is unsuccessful
-

## Request body:  ##

>>{ 

>>"email": "shura@mail.ru",

>>"password": "Shura123"

>>}

Invalid request: 

- Status: 404 Not Found

>>{

>>"code": 1,

>>"type": "error",

>>"message": "User not found"

>>}

***

## Request body:  ##

>>{ 

>>"email": "lily.mail.ru",

>>"password": "Qwerty123"

>>}

- Status: 400 Bad Request

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Email is invalid"

>>}


***

## Request body:  ##

>>{ 

>>"email": "lily@gmail.com",

>>"password": "кверти123"

>>}

- Status: 400 Bad Request

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Password is invalid"

>>}

***

## Request body:  ##

>>{ 

>>"email": "lily@gmail.com",

>>"password": "qwerty123"

>>}

- Status: 401 Unauthorized

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Password is invalid"

>>}

***

## Request body:  ##

>>{ 

>>"email": "spam@gmail.com",

>>"password": "SPAM1000"

>>}

- Status: 403 Forbidden

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Access is forbidden"

>>}

***

## Request body:  ##

>>{ 

>>"email": "",

>>"password": "Qwerty123"

>>}


- Status: 400 Bad Request

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Email is empty"

>>}

***

## Request body:  ##

>>{ 

>>"email": "lily@gmail.com",

>>"password": ""

>>}


- Status: 400 Bad Request

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Password is empty"

>>}

***
## Request body:  ##

>>{ 

>>"email": "",

>>"password": ""

>>}


- Status: 400 Bad Request

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Email and password is empty"

>>}


***

## 2) Registration page ##
>>* mapping: /auth
>>* http POST-request
>>* format JSON 
>>* Validation:

>>{

>>String login = user's login (not empty, not null, latin letters, numbers and special characters allowed),

>>String email = user's email (not empty, not null),

>>String password = user's password (not empty, not null, first capital letter, 8-16 symbols, numbers required, special characters forbidden),

>>String passwordConfirm = password,

>>String phoneNumber = user's number (not empty, not null, 11-12 numbers, special characters allowed),

>>String companyName = "" (optional)

>>}

***
User's data registration example: 
-

## Request body:  ##

>>{ 

>> "login": "lily", 

>> "email": "lily93@gmail.com",

>> "password": "Qwerty1234", 

>> "passwordConfirm": "Qwerty1234",

>> "phone": "+38(098)6754673", 

>> "company": ""

>>}


***
If registration successful
-

Successful request:

- Status: OK

>>{

>>"code": 201,

>>"type": "unknown",

>>"message": "User is created"

>>}
***

If registration unsuccessful
- 
Invalid requests:

## Request body:  ##

>>{ 

>> "login": "lyly", 

>> "email": "lily93@gmail.com",

>> "password": "Qwerty1234", 

>> "passwordConfirm": "Qwerty1234",

>> "phone": "+38(098)6754673", 

>> "company": ""

>>}


- Status: 400 Bad Request

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Invalid login supplied"

>>}
***

## Request body:  ##

>>{ 

>> "login": "lily", 

>> "email": "lily93@mail.com",

>> "password": "Qwerty1234", 

>> "passwordConfirm": "Qwerty1234",

>> "phone": "+38(098)6754673", 

>> "company": ""

>>}

- Status: 400 Bad Request

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Invalid email supplied"

>>}
***

## Request body:  ##

>>{ 

>> "login": "lily", 

>> "email": "lily93@mail.com",

>> "password": "qwerty1234", 

>> "passwordConfirm": "Qwerty1234",

>> "phone": "+38(098)6754673", 

>> "company": ""

>>}

- Status: 400 Bad Request

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Invalid password supplied"

>>}
***

## Request body:  ##

>>{ 

>> "login": "lily", 

>> "email": "lily93@mail.com",

>> "password": "Йцуке123", 

>> "passwordConfirm": "Qwerty1234",

>> "phone": "+38(098)6754673", 

>> "company": ""

>>}

- Status: 400 Bad Request

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Passwords don't match"

>>}
***

## Request body:  ##

>>{ 

>> "login": "lily", 

>> "email": "lily93@mail.com",

>> "password": "Йцуке123", 

>> "passwordConfirm": "Qwerty1234",

>> "phone": "+38-(098)6754673", 

>> "company": ""

>>}
- Status: 400 Bad Request

>>{

>>"code": 1,

>>"type": "error",

>>"message": "Invalid phone number supplied"

>>}
***

## Request body:  ##

>>{ 

>> "login": "lily", 

>> "email": "lily93@gmail.com",

>> "password": "Qwerty1234", 

>> "passwordConfirm": "Qwerty1234",

>> "phone": "+38(098)6754673", 

>> "company": ""

>>}

- Status: 409 Conflict

>>{

>>"code": 1,

>>"type": "error",

>>"message": "User with such data is already exists"

>>}

***
Database Tables Structure
-

![alt text](https://fv2-3.failiem.lv/thumb_show.php?i=2jv3m9j9q&view)